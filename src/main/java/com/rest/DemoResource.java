package com.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.rest.entity.DemoEntity;
import com.rest.service.DemoService;

@Path("/persist")
public class DemoResource {
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DemoEntity getDemoEntity() {
		return DemoService.getInstance().generateFakeData();
	}

}
