package com.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/rest/*")
public class JerseyApplication extends ResourceConfig {
	
	public JerseyApplication() {
		packages("com.tutorialacademy.rest");
	}

}
