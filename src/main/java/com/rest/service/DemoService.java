package com.rest.service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import com.rest.entity.DemoEntity;

public class DemoService {
	
	private static DemoService instance;
	
	private DemoService() { }
	
	public static DemoService getInstance() {
		if (instance == null) {
			instance = new DemoService();
		}
		return instance;
	}
	
	/*---------------------------------------------*/
	
	EntityManager em = Persistence
			.createEntityManagerFactory("HelloWorldPU")
			.createEntityManager();
	
	public DemoEntity generateFakeData() {
		DemoEntity result = new DemoEntity();
		em.persist(result);
		return result;
	}

}
